# Coinmon

Ein Monster Kampf Spiel auf der Etherium Blockchain. 
Es benutzt das Truffle Framework zur Interaktion mit den Smart Contracts der Blockchain und Angular Framework zur Visualisierung. 
Coinmon ist im Rahmen des Blockchain Projekts der [Hochschule Karlsruhe](https://hs-karlsruhe.de) beim Lehrstuhl für Verteilte Systeme entstanden.

## Voraussetzungen

Um Coinmon auszuführen ist eine Installation von [Node.js](https://nodejs.org) vorrausgesetzt. 
Dies beinhaltet `npm`, das benötigt wird die um die notwendigen Abhängigkeiten zu installieren.
Zur Installation der Abhängigkeiten ist auch [Python](https://www.python.org) (Version 2.7.x) und [git](https://git-scm.com/downloads) nötig. 
Das [MetaMask](https://metamask.io/) Plugin für Chrome wird auch benötigt.

Ein Ehterum Konto wird auch vorrausgesetzt.

## Building

1. Installiere truffle, Angular CLI und ein Ethereum client wie [geth](https://geth.ethereum.org/). 
Falls keine Test-Umgebung für Blockchain Anwendungen verfügbar ist, empfehlen wir die Verwendung von ganache-cli
  ```bash
  npm install -g truffle
  npm install -g @angular/cli
  npm install -g ganache-cli
  ```
2. Downloade die Applikation.
  ```bash
  git clone https://gitlab.com/jakaZ0806/coinmon.git
  ```

3. Führe den Ethereum Client aus. Für die Ganache CLI:
  ```bash
  ganache-cli
  ```
Notiere die 12-teilige Mnemonik-Phrase der beim Start ausgebenen wird, dieser wird später benöigt.

4. Kompiliere and migriere die Verträge.
  ```bash
  truffle compile && truffle migrate
  ```
## Konfiguration
1. Um sich mit dem Ethereum Netwerk zu verbinden, ist es nötig MetaMask zu konfigurieren.
2. Loge in die `ganache-cli` Test Accounts in MetaMask, mit vorherig ausgebenen die 12-teilige Phrase.
    1. Eine detailierte Erklärung, wie das zu tun ist kann [hier](http://truffleframework.com/docs/advanced/truffle-with-metamask#using-the-browser-extension) gefunden werden.
        1. Normalerweise, ändern sich die verfügbaren Test-Accounts bei jedem Neustart von `ganache-cli`.
        2. Um immer die selben Test-Accounts zu erhalten, wenn `ganache-cli` gestartet wird, muss das Starten mit einem Seed erfolgen wie hier: `ganache-cli --seed 0` oder `ganache-cli -m "füge deine Mnemonik Phrase hier ein benötigt zwölf Worte um mit MetaMask zu funktionieren"`
3. Verbinde MetaMask mit `ganache-cli` durch das verbinden zur Netzwerkadresse `localhost:8545`

## Ausführung
 
1. Führe die App mit der Angular CLI aus:
  ```bash
  npm start
  ```
Die App ist nun auf localhost:4200 verfügbar.
 
2. Stelle sicher das Metamask konfiguriert ist! Rufe http://localhost:4200 im browser auf.
 
3. Viel Spaß!
 
## Aufbau
 
Coinmon besteht aus zwei Teilen, einem Backend das mit Smart Contracts aufgebaut wird auf der Blockchain und einem Frontend das über einem Ethereum Client mit dem Backend kommuniziert.
 
### Smart Contracts
 
Das Backend wird mit Hilfe von zwei Smart Contracts realisiert. Ein Vertrag ist schlicht nur für das Deployment. 
Der Hauptvertrag des Backends regelt die Spiellogik über die Blockchain.   

* **Gamemaker:**  Hauptvertrag des Backends. Dieser Vertrag regelt die Spiellogik und das Matchmaking. 
Ein Kampf wird mit Hilfe von Währung abgebildet und eine Aktion führt eine Transaktion auf dieser Währung aus. 
Die Währung wird von einem Gamemaster (Void) verwaltet und den Spielern zugeteilt anhand ihrer Einstellungen.

* **Migrations:** Zuständig für das Deployment auf der Blockchain. 
 
#### Gamemaker
  
<...> = Template, für Befehl ausfüllen wie im Beispiel
 
##### Coins: 
* **TemplateCoin1:** Health
* **TemplateCoin2:** Attack
* **TemplateCoin3:** Defense
* **TemplateCoin4:** Accuracy
* **TemplateCoin5:** Stamina
 
Attacken sind intern in Attackfragmente (sprich einzelne Transactionen) zerlegt. Diese können folgende Ziele haben:
* *Den Attackinitiator*
* *Den primären Empfänger*
* *Den Void*
* *Alle Addressen im Spiel ausser Attackinitiator und Void*
 
MatchID wird ab 1 vergeben!
 
Wenn bei exeuteAttack abgebrochen wird, wird aktuell ncht zurueckgerollt! 
Attackfragmente sind dementsprechend beim Erstellen neuer Attacke im Backend anzuordnen.
 
executeAttack liefert einen (sehr) ausfuehrlichen Log über das Ergebnis einer Attacke nach folgendem Arraymuster der Laenge n:
```bash
Index:	
0	address1
1	result coin1 operations to address1
2	result coin2 operations to address1
3	result coin3 operations to address1
4	result coin4 operations to address1
5	result coin5 operations to address1
6	0 ( = Trennzeichen)
7	address2
8	result coin1 operations to address2
9	result coin2 operations to address2
...	...
...	...
...	...
n	result endFlag


------------------------------------

Moegliche Werte als Results an Indexen:
1 = Alles ok gelaufen
2 = Grenze erreicht, wurde aber noch durchgefuehrt (Bsp.: Die maximale Anzahl an wurde erreicht)
3 = Grenze erreicht, Attacke wird gecancelt (Bsp.: Die Attacke haette zu einem negativen Staminawert gefuehrt)
4 = Grenze erreicht, kritisch (Bsp.: Die Attacke hat beim Gegener zu 0>= Healthpoints gefuehrt, sprich K.O.)

Moegliche Endflags:
endFlag = 1 -> Alles ok, Result 3 nie aufgetreten
endFlag = 2 -> Result 3 irgendwo aufgetreten, Attacke wurde gecancelt
```
 
##### DEBUGGEN:
 
###### +++ Abfragen der einzelnen Coin-Werte von Accounts +++

```bash
 TemplateCoin<CoinNummer>.deployed().then(function(inst) { return inst.balanceOf(<account>);});
```

Bsp.:
TemplateCoin1.deployed().then(function(inst) { return inst.balanceOf(web3.eth.accounts[2]);});
 
######  +++ Abfragen der einzelner auswaehlbarer Attacken im GameMaker +++

```bash 
 GameMaker.deployed().then(function(inst) { return inst.attacks(<Index>);});
```
 
Bsp.:
GameMaker.deployed().then(function(inst) { return inst.attacks(1);});
  
###### +++ Abfragen der einzelner auswaehlbarer Profile im GameMaker +++

```bash 
 GameMaker.deployed().then(function(inst) { return inst.userProfiles(<Index>);});
```
 
Bsp.:
GameMaker.deployed().then(function(inst) { return inst.userProfiles(1);});
 
#####  SPIEL STEUERN:
 
###### +++ Spiel starten +++

```bash
 GameMaker.deployed().then(function(inst) { return inst.startGame(<Spieleradressen als Array>, <Profilauswahl der Spieler als Array>);});
```

Bsp.:
var players = [web3.eth.accounts[1], web3.eth.accounts[2], web3.eth.accounts[3]];
var profiles = [1,2,3];
 
var res = GameMaker.deployed().then(function(inst) { return inst.startGame(players, profiles);});
  
######  +++ Attacke ausfuehren +++

```bash
 GameMaker.deployed().then(function(inst) { return inst.executeAttack(<ID des zu startenden Angriffs>, <ID des Matches in dem die Attacke zu starten ist>, <Adresse des Initiators der Attacke>, <Adresse des primaeren(!) Empfaengers dr Attacke>);});
```

Bsp.:
var res = GameMaker.deployed().then(function(inst) { return inst.executeAttack(1, 1, web3.eth.accounts[0], web3.eth.accounts[1]);});
 
######  +++ Beendet ein Spiel und sammelt alle Coins ein +++

```bash 
 GameMaker.deployed().then(function(inst) { return inst.endGame(<Alle Spieleraddressen des Matches>);});
```
 
Bsp.:
var res = GameMaker.deployed().then(function(inst) { return inst.endGame(players);});
 
### Front End

Dient zur Visualierung des aktuellen Zustand eines Spiels. Die Spieler können durch das Frontend Transkationen an das Backend senden. 
Diese starten dann öffentliche Funktionen des Gamemakers Backends.
