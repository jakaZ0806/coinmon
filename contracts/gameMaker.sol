pragma solidity ^0.4.24;





contract TemplateCoin {

    string public name;
    string public symbol;
    address public voidAddress;
	uint8 public decimals = 18;
    uint256 public totalSupply;
	uint256 public upperLimit;
	uint256 public lowerLimit;
	bool public strictCut;
	bool public isExistential;

    mapping (address => uint256) public balanceOf;

    event Transfer(address indexed from, address indexed to, uint256 value);

    event Burn(address indexed from, uint256 value);

    constructor(
        uint256 initialSupply,
        string tokenName,
        string tokenSymbol,
		address voidAddressPara,
		uint256 lowerLimitPara,
		uint256 upperLimitPara,
		bool strictCutPara,
		bool isExistentialPara
    ) public {
        totalSupply = initialSupply * 10 ** uint256(decimals);
        balanceOf[msg.sender] = totalSupply;
        name = tokenName;
        symbol = tokenSymbol;
		voidAddress = voidAddressPara;
		lowerLimit = lowerLimitPara;
		upperLimit = upperLimitPara;
		strictCut = strictCutPara;
		isExistential = isExistentialPara;
    }

	function transfer(address _from, address _to, uint256 _value) public returns (uint256) {

		uint256 result = 1;

		require(_from == voidAddress || _to == voidAddress, 'no void present in transaction');
		require(_to != 0x0, 'one address is 0');
		require(balanceOf[_to] + _value >= balanceOf[_to], 'sent sum is zero');

		if(isExistential == true && (balanceOf[_from] - _value <= lowerLimit|| balanceOf[_from] < _value))	{
			result = 4;
			_value = balanceOf[_from] - lowerLimit;
		}

		if(balanceOf[_from] - _value < lowerLimit || balanceOf[_from] < _value)	{
			if(strictCut == true)	{
				result = 3;
			}
			else	{
				result = 2;
				_value = balanceOf[_from] - lowerLimit;
			}
		}

		if(balanceOf[_to] + _value > upperLimit && _to != voidAddress)	{
			if(strictCut == true)	{
				result = 3;
			}
			else	{
				result = 2;
				_value = upperLimit - balanceOf[_to];
			}
		}

		if(result != 3)	{
			uint previousBalances = balanceOf[_from] + balanceOf[_to];

			balanceOf[_from] -= _value;

			balanceOf[_to] += _value;
			emit Transfer(_from, _to, _value);

			assert(balanceOf[_from] + balanceOf[_to] == previousBalances);
    	}

		return result;
    }
}




contract TemplateCoin1 is TemplateCoin {
	constructor(uint256 initialSupply, string tokenName, string tokenSymbol, address voidAddressPara, uint256 lowerLimit, uint256 upperLimit, bool strictCut, bool isExistential) TemplateCoin(initialSupply, tokenName, tokenSymbol, voidAddressPara, lowerLimit, upperLimit, strictCut, isExistential) public {}
}
contract TemplateCoin2 is TemplateCoin {
	constructor(uint256 initialSupply, string tokenName, string tokenSymbol, address voidAddressPara, uint256 lowerLimit, uint256 upperLimit, bool strictCut, bool isExistential) TemplateCoin(initialSupply, tokenName, tokenSymbol, voidAddressPara, lowerLimit, upperLimit, strictCut, isExistential) public {}
}
contract TemplateCoin3 is TemplateCoin {
	constructor(uint256 initialSupply, string tokenName, string tokenSymbol, address voidAddressPara, uint256 lowerLimit, uint256 upperLimit, bool strictCut, bool isExistential) TemplateCoin(initialSupply, tokenName, tokenSymbol, voidAddressPara, lowerLimit, upperLimit, strictCut, isExistential) public {}
}
contract TemplateCoin4 is TemplateCoin {
	constructor(uint256 initialSupply, string tokenName, string tokenSymbol, address voidAddressPara, uint256 lowerLimit, uint256 upperLimit, bool strictCut, bool isExistential) TemplateCoin(initialSupply, tokenName, tokenSymbol, voidAddressPara, lowerLimit, upperLimit, strictCut, isExistential) public {}
}
contract TemplateCoin5 is TemplateCoin {
	constructor(uint256 initialSupply, string tokenName, string tokenSymbol, address voidAddressPara, uint256 lowerLimit, uint256 upperLimit, bool strictCut, bool isExistential) TemplateCoin(initialSupply, tokenName, tokenSymbol, voidAddressPara, lowerLimit, upperLimit, strictCut, isExistential) public {}
}




contract ProfilePool {

	struct Profile {
		uint16 id;
		string name;
		uint16 health;
		uint16 attack;
		uint16 defense;
		uint16 accuracy;
		uint16 stamina;
		uint16[] attacks;
	}

	Profile[] profiles;


	constructor()  {

	    uint16[] memory attacks = new uint16[](4);
	    attacks[0] = 1;
	    attacks[1] = 2;
	    attacks[2] = 3;
	    attacks[3] = 4;

		profiles.push(Profile(0, 'default', 100, 50, 50, 75, 250, attacks));
		profiles.push(Profile(1, 'tank', 250, 10, 75, 75, 250, attacks));
		profiles.push(Profile(2, 'berserker', 200, 90, 5, 60, 350, attacks));
		profiles.push(Profile(3, 'sniper', 80, 50, 50, 100, 10, attacks));
	}


	function getIdentifiers() constant public returns (uint16[])	{

		uint16[] memory ids = new uint16[](profiles.length);

		for(uint16 i = 0; i < profiles.length; i++)	{
			ids[i] = profiles[i].id;
		}

		return ids;
	}


	function getInfo(uint16 id) constant public returns (uint16, string, uint16, uint16, uint16, uint16, uint16, uint16[])	{

		uint16[] memory profileAttacks = new uint16[](1);
		Profile memory thisProfile = Profile(0,"Error: no profile for this id!",0,0,0,0,0,profileAttacks);
		for(uint256 reusableInt = 0; reusableInt < profiles.length; reusableInt++)	{
			if(profiles[reusableInt].id == id)	{
				thisProfile = profiles[reusableInt];
			}
		}

		return (id, thisProfile.name, thisProfile.health, thisProfile.attack, thisProfile.defense, thisProfile.accuracy, thisProfile.stamina, thisProfile.attacks);
	}


    function getAllProfilesPart1() constant public returns (uint16[], uint16[], uint16[], uint16[], uint16[], uint16[]) {

		uint16[] memory ids = new uint16[](profiles.length);
		uint16[] memory healths = new uint16[](profiles.length);
		uint16[] memory attacks = new uint16[](profiles.length);
		uint16[] memory defenses = new uint16[](profiles.length);
		uint16[] memory accuracies = new uint16[](profiles.length);
		uint16[] memory staminas = new uint16[](profiles.length);


		for(uint256 i = 0;  i < profiles.length; i++)	{
			ids[i] = profiles[i].id;
			healths[i] = profiles[i].health;
			attacks[i] = profiles[i].attack;
			defenses[i] = profiles[i].defense;
			accuracies[i] = profiles[i].accuracy;
			staminas[i] = profiles[i].stamina;
		}

		return (ids, healths, attacks, defenses, accuracies, staminas);
	}

	function getAllProfilesPart2() constant public returns (uint16[]) {

		uint256 attackArrayLength = 0;

		for(uint256 i = 0; i < profiles.length; i++)	{
			attackArrayLength += profiles[i].attacks.length;
		}

		uint16[] memory profileAttacks = new uint16[](attackArrayLength + profiles.length - 1);

        uint256 profileTrack = 0;
        uint256 attackTrack = 0;

		for(i = 0;  i < profileAttacks.length ; i++)	{

		    if(profiles[profileTrack].attacks.length == attackTrack)	{

				profileAttacks[i] = 0;
				i++;
				profileTrack++;
				attackTrack = 0;
			}

		    profileAttacks[i] = profiles[profileTrack].attacks[attackTrack++];
		}

		return (profileAttacks);
	}
}




contract AttackPool {

	struct TransactionOrder {
		uint16 coin;
		uint16 from;
		uint16 to;
		uint256 ammount;
	}

	struct Attack {
		uint16 attackId;
		string name;
		string description;
		uint16 transactionsStart;
		uint16 transactionsEnd;
	}

	Attack[] public attacks;
	TransactionOrder[] public transactions;


	// Coins (= 1st para):
	// 0 = health
	// 1 = attack
	// 2 = defense
	// 3 = accuracy
	// 4 = stamina
	//
	// Targets (= 2nd & 3rd para):
	// 1 = void
	// 2 = me
	// 3 = enemy_one
	// 4 = enemy_all
	//
	// Id 0 in NOT allowed! (serves as divider in return array
	constructor()  {

		uint16 start = 0;

        transactions.push(TransactionOrder(0, 3, 1, 50));
		transactions.push(TransactionOrder(4, 2, 1, 50));

		attacks.push(Attack(1, 'Sword swing', "Deals 50 Core Damage to an Enemy. Costs 50 Stamina.", start, uint16(transactions.length - 1)));
		start = uint16(transactions.length);

		transactions.push(TransactionOrder(2, 1, 2, 20));
		transactions.push(TransactionOrder(4, 2, 1, 30));

		attacks.push(Attack(2, 'Defense Formation', "Increases your Defense by 20 Points. Costs 30 Stamina.", start, uint16(transactions.length - 1)));
		start = uint16(transactions.length);

		transactions.push(TransactionOrder(0, 3, 1, 20));
		transactions.push(TransactionOrder(0, 1, 2, 20));
		transactions.push(TransactionOrder(4, 2, 1, 50));

		attacks.push(Attack(3, 'Vampire Spell', "Transfers 20 Health from an enemy to you. Costs 50 Stamina.", start, uint16(transactions.length - 1)));
		start = uint16(transactions.length);

		transactions.push(TransactionOrder(0, 4, 1, 65));
		transactions.push(TransactionOrder(4, 2, 1, 150));

		attacks.push(Attack(4, 'Explosion Spell', "Deals 65 Core Damage to all enemies. Costs 150 Stamina.", start, uint16(transactions.length - 1)));
	}


	function getIdentifiers() constant public returns (uint16[])	{

		uint16[] memory ids = new uint16[](attacks.length);

		for(uint16 i = 0; i < attacks.length; i++)	{
			ids[i] = attacks[i].attackId;
		}

		return ids;
	}


	function getInfo(uint256 id) constant public returns (uint256, string, string)	{

		Attack memory thisAttack = Attack(0,"","Error: no attack for this id!",0,0);
		for(uint256 reusableInt = 0; reusableInt < attacks.length; reusableInt++)	{
			if(attacks[reusableInt].attackId == id)	{
				thisAttack = attacks[reusableInt];
			}
		}

		return (id, thisAttack.name, thisAttack.description);
	}


	function getAllAttacks() constant public returns (uint16[], uint16[], uint16[], uint16[], uint256[]) {

		uint16[] memory id = new uint16[](transactions.length);
		uint16[] memory coin = new uint16[](transactions.length);
		uint16[] memory from = new uint16[](transactions.length);
		uint16[] memory to = new uint16[](transactions.length);
		uint256[] memory ammount = new uint256[](transactions.length);

		uint256 attackCount = 0;
		uint256 nextAttack = attacks[0].transactionsEnd + 1;

	    id[0] = attacks[0].attackId;

		for(uint256 i = 0;  i < transactions.length; i++)	{

			if(i == nextAttack)   {
				attackCount++;
				nextAttack = attacks[attackCount].transactionsEnd + 1;
			}

			id[i] = attacks[attackCount].attackId;

			coin[i] = transactions[i].coin;
			from[i] = transactions[i].from;
			to[i] = transactions[i].to;
			ammount[i] = transactions[i].ammount;
		}

		return (id, coin, from, to, ammount);
	}
}




contract GameMaker {

	struct Profile {
		uint16 id;
		uint16 health;
		uint16 attack;
		uint16 defense;
		uint16 accuracy;
		uint16 stamina;
		uint16[] attacks;
	}

	struct Transaction{
		uint16 coin;
		address from;
		address to;
		uint256 ammount;
	}

	struct Attack {
		uint16 attackId;
		uint16 transactionsStart;
		uint16 transactionsEnd;
	}

	struct TransactionOrder {
		uint16 coin;
		uint16 from;
		uint16 to;
		uint256 ammount;
	}

	struct Match {
		uint256 id;
		address[] players;
		bool[] over;
		uint256[] profiles;
		uint16 nextPlayer;
		string message;
	}

  event GameStarted (
		uint256 matchCount,
		address[] players,
    uint256[] profiles
	);


	// Knowledge of the different addresses.
    TemplateCoin[] public coinContracts;
    AttackPool public attackContract;
    ProfilePool public profileContract;

	address public voidAddress;

	mapping (uint256 => Match) public matches;
	uint256 matchCount;

	mapping (uint16 => Profile) public userProfiles;
	mapping (uint16 => Attack) public attacks;
	TransactionOrder[] public attackTransactions;

	constructor(
        address healthContractAddress,
        address attackContractAddress,
        address defenseContractAddress,
        address accuracyContractAddress,
        address staminaContractAddress,
        address attacksContractAddress,
        address profileContractAddress,
        address voidAddressPara
    ) public {

		coinContracts.push(TemplateCoin1(healthContractAddress)); // 'health'
        coinContracts.push(TemplateCoin2(attackContractAddress)); // 'attack'
        coinContracts.push(TemplateCoin3(defenseContractAddress)); // 'defense'
        coinContracts.push(TemplateCoin4(accuracyContractAddress)); // 'accuracy'
        coinContracts.push(TemplateCoin5(staminaContractAddress)); // 'stamina'

		attackContract = AttackPool(attacksContractAddress);
        profileContract = ProfilePool(profileContractAddress);
        voidAddress = voidAddressPara;

		matchCount = 0;

		updateAttacks();
		updateProfiles();
	}

	function getPlayers(uint256 matchId) public constant returns (address[])	{
		return matches[matchId].players;
	}


	function updateAttacks()	{

		var (ids, coins, froms, tos, ammounts) = attackContract.getAllAttacks();

	    uint16 lastId = ids[0];
		uint16 lastIndex = 0;

	    for(uint16 i = 0; i < coins.length; i++) {

	        if(lastId != ids[i])    {
				attacks[ids[i-1]] =  Attack(ids[i-1], lastIndex, uint16(attackTransactions.length - 1));
				lastId = ids[i];
				lastIndex = uint16(attackTransactions.length);
			}

			attackTransactions.push(TransactionOrder(coins[i], froms[i], tos[i], ammounts[i]));
		}

		attacks[ids[ids.length - 1]] = Attack(ids[ids.length - 1], lastIndex, uint16(attackTransactions.length - 1));
	}


	function updateProfiles ()	{

		var (ids, healths, reAttacks, defenses,accuracies,staminas) = profileContract.getAllProfilesPart1();
		var profileAttacks = profileContract.getAllProfilesPart2();

		uint16 arrayTrack = 0;
		uint16[] memory arrayLengthTrack = new uint16[](ids.length);
		arrayLengthTrack[0] = 0;

		for(uint16 i = 0; i < profileAttacks.length; i++)	{


			if(profileAttacks[i] == 0)	{
				arrayTrack++;
				arrayLengthTrack[arrayTrack] = 0;
			}
			else {
				arrayLengthTrack[arrayTrack]++;
			}
		}

		arrayTrack = 0;

		for(i = 0; i < ids.length; i++)	{

			uint16[] memory profileAttack = new uint16[](arrayLengthTrack[i]);

			for(uint16 j = 0; j < profileAttack.length; j++)	{
				profileAttack[j] = profileAttacks[arrayTrack + j];
			}

			arrayTrack += arrayLengthTrack[i];

			userProfiles[ids[i]] = Profile(ids[i], healths[i], reAttacks[i], defenses[i], accuracies[i], staminas[i], profileAttack);
		}
	}


	function getParticipantsFromOrder(TransactionOrder order, address sender, address primaryTarget, address[] context, bool forSender) constant internal returns (address[])	{

		uint16 track = 0;
		address[] memory participants;

		uint16 orderCode = order.to;
		if(forSender)	{
			orderCode = order.from;
		}

		if(orderCode == 4)	{

			participants = new address[](context.length - 1);

			for(uint j = 0; j < context.length; j++)	{
				if(context[j] != sender)	{
					participants[track++] = context[j];
				}
			}
		}
		else	{

			participants = new address[](1);

			if(orderCode == 3)	{
				participants[0] = primaryTarget;
			}
			else if(orderCode == 2)	{
				participants[0] = sender;
			}
			else if(orderCode == 1)	{
				participants[0] = voidAddress;
			}
		}

		return participants;
	}


	function resolveTransactionOrder(TransactionOrder order, address sender, address primaryTarget, address[] context) internal constant returns (Transaction[])	{

		address[] memory senders = getParticipantsFromOrder(order, sender, primaryTarget, context, true);
		address[] memory targets = getParticipantsFromOrder(order, sender, primaryTarget, context, false);

		Transaction[] memory transactions = new Transaction[](senders.length * targets.length);

		for(uint16 j = 0; j < senders.length; j++)	{
			for(uint16 k = 0; k < targets.length; k++)	{

				transactions[j * targets.length + k] = Transaction(order.coin, senders[j], targets[k], order.ammount);

				if(transactions[j * targets.length + k].coin == 0)	{

					uint256 attackCoins = coinContracts[1].balanceOf(sender);

					uint256 defenseCoins = coinContracts[2].balanceOf(transactions[j * targets.length + k].from);
					if(order.from == 1)	{
						defenseCoins = coinContracts[2].balanceOf(primaryTarget);
					}

					if(attackCoins <= defenseCoins)	{
						transactions[j * targets.length + k].ammount = transactions[j * targets.length + k].ammount * (100 - (defenseCoins - attackCoins) + (defenseCoins - attackCoins) / 2) / 100;
					}
					else	{
						transactions[j * targets.length + k].ammount = transactions[j * targets.length + k].ammount * (attackCoins - defenseCoins + 100) / 100;
					}
				}
			}
		}

		return transactions;
	}


	function fireTransactions(Transaction[] transactions, address[] players) internal returns (uint256[]) {

		uint256[] memory results = new uint256[]((players.length + 1) * 7);
		uint16 addressIndex = 999;
		uint256 i = 0;

		while(i < results.length)	{

			if(i < players.length * 7)	{
				results[i++] = uint256(players[i/7]);
			}
			else	{
				results[i++] = uint256(voidAddress);
			}
			results[i++] = 1;
			results[i++] = 1;
			results[i++] = 1;
			results[i++] = 1;
			results[i++] = 1;
			i++;
		}

		results[results.length - 1] = 1;

		for(i = 0; i < transactions.length && results[results.length - 1] == 1; i++)	{

			addressIndex = 999;
			for(uint16 j = 0; j < results.length && addressIndex == 999; j = j + 7)	{
				if(results[j] == uint256(transactions[i].to))	{
					addressIndex = j;
				}
			}

			results[addressIndex + transactions[i].coin + 1] = coinContracts[transactions[i].coin].transfer(transactions[i].from, transactions[i].to, transactions[i].ammount);

			if(results[addressIndex + transactions[i].coin] == 3)	{
				results[results.length - 1] = 2;
			}
		}

		return results;
	}


	event attackResult(
		address me,
		uint256[] info
	);

	//
	//	Result:
	//
	//	address1
	//  result coin1 operations to address1
	//  result coin2 operations to address1
	//  result coin3 operations to address1
	//  result coin4 operations to address1
	//  result coin5 operations to address1
	//	0
	//	address2
	//  result coin1 operations to address2
	//  result coin2 operations to address2
	//  result coin3 operations to address2
	//  result coin4 operations to address2
	//  result coin5 operations to address2
	//	0
	//	address3
	//	...
	//	...
	//	...
	//  result endFlag
	//
	//
	//	------------------------------------
	//
	//	1 = ok
	// 	2 = reached border, but got through
	//	3 = reached border, canceled transactions
	//	4 = reached border, critical flag
	//
	//	endFlag = 1 -> ok
	//  endFlag = 2 -> got cancelled
	//
	//
	function executeAttack(uint16 attackId, uint256 matchId, address sender, address primaryTarget) public returns (uint256[])	{

		uint256[] memory result = new uint256[]((matches[matchId].players.length + 1) * 7);
		result[result.length - 1] = 3;

		if(matches[matchId].players[matches[matchId].nextPlayer] == sender)	{

			result[result.length - 1] = 1;

			for(uint16 j = attacks[attackId].transactionsStart; j <= attacks[attackId].transactionsEnd && result[result.length - 1] != 2; j++)	{

				Transaction[] memory resolvedTransactions = resolveTransactionOrder(attackTransactions[j], sender, primaryTarget, matches[matchId].players);

				uint256[] memory originalResult = fireTransactions(resolvedTransactions, matches[matchId].players);

				for(uint256 k = 0; k < originalResult.length - 1; k++)	{
					if(result[k] != 1 && originalResult[k] != 1)	{
						result[k] = originalResult[k];
					}
				}

				if(originalResult[originalResult.length - 1] == 2)	{
					result[result.length - 1] = 2;
				}
			}

			if(result[result.length - 1] != 2)	{
				matches[matchId].nextPlayer++;
				matches[matchId].nextPlayer = matches[matchId].nextPlayer % uint16(matches[matchId].players.length);
			}
		}

		emit attackResult(sender, result);
		return result;
	}


	function setGameCoins(bool forStart, address[] players, uint256[] profileSelection) internal returns (Transaction[])	{

		Transaction[] memory transactions = new Transaction[](players.length * 5);

		uint256 i = 0;
		uint256[] memory values = new uint256[](5);

		while(i < transactions.length)	{

			address from = players[i/5];
			address to = voidAddress;

			if(forStart)	{

				Profile memory userProfile = userProfiles[uint16(profileSelection[i/5])];
				to = players[i/5];
				from = voidAddress;

				values[0] = userProfile.health;
				values[1] = userProfile.attack;
				values[2] = userProfile.defense;
				values[3] = userProfile.accuracy;
				values[4] = userProfile.stamina;
			}

			for(uint16 j = 0; j < 5; j++)	{
				if(!forStart)	{
					values[j] = coinContracts[j].balanceOf(from);
				}
				transactions[i++] = Transaction(j, from, to, values[j]);
			}
		}

		fireTransactions(transactions, players);
	}

	function startGame(address[] players, uint256[] profileSelection) public returns (uint256)	{

		bool[] memory stillParticipating = new bool[](players.length);
		for(uint16 i = 0; i < stillParticipating.length; i++)	{
			stillParticipating[i] = true;
		}
		matchCount++;
		matches[matchCount] = Match(matchCount, players, stillParticipating, profileSelection, 0, "No action yet.");
		setGameCoins(true, players, profileSelection);
		emit GameStarted(matchCount, players, profileSelection);
		return matchCount;
	}


	function endGame(address[] players) public {

		uint256[] memory profileSelectionMock = new uint256[](0);
		setGameCoins(false, players, profileSelectionMock);
	}
}





contract GamePlaner	{

	struct Sleeper	{
		address player;
		uint256 profile;
		uint256 timestamp;
		uint256 matchId;
	}

	GameMaker gameMaker;
	Sleeper[] public searchingPlayers;
	Sleeper[] newSearchingPlayers;
	uint256 MAX_DELAY = 60;

	constructor(address gameMakerAddress)	{
		gameMaker = GameMaker(gameMakerAddress);
	}

	event MatchInfo(
		address me,
		uint256 info,
		address[] players
	);


	function updateTimestamp(address player, uint256 profile) public returns (uint256)	{

		uint256 returner = 0;
		delete newSearchingPlayers;
		bool alreadyExists = false;
		address[] memory players = new address[](2);
		players[0] = player;

		for(uint256 i = 0; i < searchingPlayers.length; i++)	{
			if(searchingPlayers[i].player == player)	{
				if(searchingPlayers[i].matchId == 0)	{
					searchingPlayers[i].timestamp = now;
					newSearchingPlayers.push(searchingPlayers[i]);
				}
				else	{
					returner = searchingPlayers[i].matchId;
					players = gameMaker.getPlayers(returner);
				}
				alreadyExists = true;
			}
			else if(searchingPlayers[i].timestamp + MAX_DELAY > now || searchingPlayers[i].matchId != 0)	{
				newSearchingPlayers.push(searchingPlayers[i]);
			}
		}

		searchingPlayers = newSearchingPlayers;

		if(!alreadyExists)	{
			searchingPlayers.push(Sleeper(player, profile, now, 0));
		}


		emit MatchInfo(player, returner, players);
		return returner;
	}


	function matchMe(address me, uint256 profile) public returns (uint256)	{

		delete newSearchingPlayers;

		address[] memory players = new address[](2);
		uint256[] memory profiles = new uint256[](2);
		players[0] = me;
		profiles[0] = profile;
		uint256 returner = 0;
		bool foundMatch = false;

		for(uint256 i = 0; i < searchingPlayers.length; i++)	{
			if(searchingPlayers[i].matchId == 0)	{
				if(searchingPlayers[i].timestamp + MAX_DELAY > now)	{
					if(!foundMatch)	{
						players[1] = searchingPlayers[i].player;
						profiles[1] = searchingPlayers[i].profile;
						searchingPlayers[i].matchId = gameMaker.startGame(players, profiles);
						returner = searchingPlayers[i].matchId;
						foundMatch = true;
					}
					newSearchingPlayers.push(searchingPlayers[i]);
				}
			}
			else	{
				newSearchingPlayers.push(searchingPlayers[i]);
			}
		}

		searchingPlayers = newSearchingPlayers;

		if(returner == 0)	{
			returner = updateTimestamp(me, profile);
		}

		emit MatchInfo(me, returner, players);
		return returner;
	}
}
