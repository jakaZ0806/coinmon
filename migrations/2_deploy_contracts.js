// ACHTUNG!: Member muss man mit () am Ende aufrufen in .sol !!

var HealthCoin = artifacts.require("templateCoin1");
var AttackCoin = artifacts.require("templateCoin2");
var DefenseCoin = artifacts.require("templateCoin3");
var AccuracyCoin = artifacts.require("templateCoin4");
var StaminaCoin = artifacts.require("templateCoin5");
var AttackPool = artifacts.require("attackPool");
var ProfilePool = artifacts.require("profilePool");
var GameMaker = artifacts.require("gameMaker");
var GamePlaner = artifacts.require("gamePlaner");


module.exports = function(deployer, network, accounts) {

	var contractCount = 7;

	function callback()	{
		if(--contractCount === 0)	{
			return deployer.deploy(GameMaker, HealthCoin.address, AttackCoin.address, DefenseCoin.address, AccuracyCoin.address, StaminaCoin.address, AttackPool.address, ProfilePool.address, accounts[0]).then(function()	{
				return deployer.deploy(GamePlaner, GameMaker.address);
			});
		}
	}

	deployer.deploy(HealthCoin, 1000000000, 'HealthPoints', 'he$', accounts[0], 0, Number.MAX_SAFE_INTEGER, true, true).then(callback);
	deployer.deploy(AttackCoin, 1000000000, 'AttackPoints', 'at$', accounts[0], 0, 100, false, false).then(callback);
	deployer.deploy(DefenseCoin, 1000000000, 'DefensePoints', 'de$', accounts[0], 0, 100, false, false).then(callback);
	deployer.deploy(AccuracyCoin, 1000000000, 'AccuracyPoints', 'ac$', accounts[0], 0, 100, false, false).then(callback);
	deployer.deploy(StaminaCoin, 1000000000, 'StaminaPoints', 'st$', accounts[0], 0, Number.MAX_SAFE_INTEGER, true, false).then(callback);
	deployer.deploy(ProfilePool).then(callback);
	deployer.deploy(AttackPool).then(callback);
}
