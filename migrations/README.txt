Aufsetzen und Benutzung:

1. npm install -g truffle
2. Ins eigene truffle-Directory gehen (sprich da wo das Project sein soll)
3. truffle init
4. 2_deploy_contracts.js in ./migrations durch .js aus Discord ersetzen.
5. gameMaker.sol in ./contracts kopieren
6. truffle compile
7. truffle develop *oeffnet truffle-interne Konsole, ab hier alles darin* 
8. migrate --reset (ohne --reset beim ersten Mal)
9. Per <ContractName>.deployed().then(function(inst) { return inst.<ContractFunktion>(<Paras>); }); <-- Auch bei Membern mit () am Ende!
   Contracts steuern 