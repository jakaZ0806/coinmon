import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../util/web3.service';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { forEach } from '../../../node_modules/@angular/router/src/utils/collection';
const profilePool_artifacts = require('../../../build/contracts/ProfilePool.json');
const attackPool_artifacts = require('../../../build/contracts/AttackPool.json');

const gameMaker_artifacts = require('../../../build/contracts/GameMaker.json');

const HealthCoin_artifacts = require('../../../build/contracts/TemplateCoin1.json');
const AttackCoin_artifacts = require('../../../build/contracts/TemplateCoin2.json');
const DefenseCoin_artifacts = require('../../../build/contracts/TemplateCoin3.json');
const AccuracyCoin_artifacts = require('../../../build/contracts/TemplateCoin4.json');
const StaminaCoin_artifacts = require('../../../build/contracts/TemplateCoin5.json');

declare let require: any;

const classPath_berseker = require('../../assets/berseker.png');
const classPath_default = require('../../assets/default.png');
const classPath_tank = require('../../assets/tank.png');
const classPath_sniper = require('../../assets/sniper.png');

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  [x: string]: any;

  accounts: string[];
  MetaCoin: any;
  AttackPool: any;
  ProfilePool: any;
  GameMaker: any;
  HealthCoin: any;
  AttackCoin: any;
  DefenseCoin: any;
  AccuracyCoin: any;
  StaminaCoin: any;
  info: any;
  coinsInitialized = 0;

  matchId = 1;
  private sub: any;

  status = '';

  enemy = {
    name: 'TestEnemy',
    hp: '70',
    attack: '0',
    defense: '0',
    accuracy: '0',
    stamina: '0',
    class: 'Default',
    maxHP: '99999',
    percentHP: 100,
    classPicturePatch: classPath_default
  };

  player = {
    name: 'TestPlayer',
    hp: '15',
    attack: '0',
    defense: '0',
    accuracy: '0',
    stamina: '0',
    class: 'Default',
    maxHP: '99999',
    percentHP: 100,
    classPicturePatch: classPath_default
  };

  model = {
    amount: 5,
    receiver: '',
    balance: 0,
    account: '',
    opponent: '',
    attacks: [
      { id: 1, name: 'Attack 0', description: 'description' },
    ]
  };

  constructor(private web3Service: Web3Service, private matSnackBar: MatSnackBar, private route: ActivatedRoute, private router: Router) {
    console.log('Constructor: ' + web3Service);
  }


  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.model.account = params['account'];
      this.model.opponent = params['enemy'];
      this.matchId = params['matchId'];

      switch (params['playerProfile']) {
        case '0':
          this.player.class = 'Default';
          this.player.classPicturePatch = classPath_default;
          break;
        case '1':
          this.player.class = 'Tank';
          this.player.classPicturePatch = classPath_tank;
          break;
        case '2':
          this.player.class = 'Berserker';
          this.player.classPicturePatch = classPath_berseker;
          break;
        case '3':
          this.player.class = 'Sniper';
          this.player.classPicturePatch = classPath_sniper;
          break;
      }
      switch (params['enemyProfile']) {
        case '0':
          this.enemy.class = 'Default';
          this.enemy.classPicturePatch = classPath_default;
          break;
        case '1':
          this.enemy.class = 'Tank';
          this.enemy.classPicturePatch = classPath_tank;
          break;
        case '2':
          this.enemy.class = 'Berserker';
          this.enemy.classPicturePatch = classPath_berseker;
          break;
        case '3':
          this.enemy.class = 'Sniper';
          this.enemy.classPicturePatch = classPath_sniper;
          break;
      }

      // In a real app: dispatch action to load the details here.
    });
    console.log('OnInit: ' + this.web3Service);
    this.web3Service.artifactsToContract(attackPool_artifacts)
      .then((AttackPoolAbstraction) => {
        this.AttackPool = AttackPoolAbstraction;
        this.getPossibleAttacks();
      });
    this.web3Service.artifactsToContract(profilePool_artifacts)
      .then((ProfilePoolAbstraction) => {
        this.ProfilePool = ProfilePoolAbstraction;
        // this.getAttacks();
      });

    this.web3Service.artifactsToContract(gameMaker_artifacts)
      .then((GameMakerAbstraction) => {
        this.GameMaker = GameMakerAbstraction;
        this.listenAttackEvents();
      });

    this.web3Service.artifactsToContract(HealthCoin_artifacts)
      .then((HealthCoinAbstraction) => {
        this.HealthCoin = HealthCoinAbstraction;
        this.coinLoaded();
      });

    this.web3Service.artifactsToContract(AttackCoin_artifacts)
      .then((AttackCoinAbstraction) => {
        this.AttackCoin = AttackCoinAbstraction;
        this.coinLoaded();
      });

    this.web3Service.artifactsToContract(DefenseCoin_artifacts)
      .then((DefenseCoinAbstraction) => {
        this.DefenseCoin = DefenseCoinAbstraction;
        this.coinLoaded();
      });

    this.web3Service.artifactsToContract(AccuracyCoin_artifacts)
      .then((AccuracyCoinAbstraction) => {
        this.AccuracyCoin = AccuracyCoinAbstraction;
        this.coinLoaded();
      });

    this.web3Service.artifactsToContract(StaminaCoin_artifacts)
      .then((StaminaCoinAbstraction) => {
        this.StaminaCoin = StaminaCoinAbstraction;
        this.coinLoaded();
      });

    this.watchAccount();
  }

  coinLoaded() {
    this.coinsInitialized++;
    if (this.coinsInitialized === 5) {
      this.getStats();
    }
  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }

  watchAccount() {
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.accounts = accounts;
      this.model.account = accounts[0];
    });
  }

  getPercent(val1, val2) {
    return (val1 / val2) * 100;
  }

  async getAttackInfo(identifier) {
    const deployedAttackPool = await this.AttackPool.deployed();
    const attacks = await deployedAttackPool.getInfo.call(identifier);
    this.model.attacks[identifier - 1] = {
      id: identifier,
      name: attacks[1].toString(10),
      description: attacks[2].toString(20)
    };
    console.log(this.model.attacks);
  }

  async getPossibleAttacks() {
    try {
      const deployedAttackPool = await this.AttackPool.deployed();
      console.log(deployedAttackPool);
      const transaction = await deployedAttackPool.getIdentifiers.call();
      if (!transaction) {
        this.setStatus('Request failed!');
      } else {
        console.log(transaction);
        transaction.forEach((identifier) => {
          this.getAttackInfo(+identifier.toString(10));
        });
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error getting attacks; see log.');
    }
  }


  async getStats() {
    try {
      const deployedHealthCoin = await this.HealthCoin.deployed();
      const playerHealth = await deployedHealthCoin.balanceOf.call(this.model.account);
      if (this.player.maxHP === '99999') {
        this.player.maxHP = playerHealth.toString(10);
      }
      this.player.hp = playerHealth.toString(10);
      this.player.percentHP = this.getPercent(+this.player.hp, +this.player.maxHP);
      if (+this.enemy.hp !== 0 && +this.player.hp === 0) {
        alert('You Lost!');
        this.router.navigate(['/lobby']);
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedAttackCoin = await this.AttackCoin.deployed();
      const playerAttack = await deployedAttackCoin.balanceOf.call(this.model.account);
      this.player.attack = playerAttack.toString(10);
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedDefenseCoin = await this.DefenseCoin.deployed();
      const playerDefense = await deployedDefenseCoin.balanceOf.call(this.model.account);
      this.player.defense = playerDefense.toString(10);
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedAccuracyCoin = await this.AccuracyCoin.deployed();
      const playerAccuracy = await deployedAccuracyCoin.balanceOf.call(this.model.account);
      this.player.accuracy = playerAccuracy.toString(10);
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedStaminaCoin = await this.StaminaCoin.deployed();
      const playerStamina = await deployedStaminaCoin.balanceOf.call(this.model.account);
      this.player.stamina = playerStamina.toString(10);
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedHealthCoin = await this.HealthCoin.deployed();
      const playerHealth = await deployedHealthCoin.balanceOf.call(this.model.opponent);
      this.enemy.hp = playerHealth.toString(10);
      if (this.enemy.maxHP === '99999') {
        this.enemy.maxHP = playerHealth.toString(10);
      }
      this.enemy.percentHP = this.getPercent(+this.enemy.hp, +this.enemy.maxHP);
      if (+this.enemy.hp === 0 && +this.player.hp !== 0) {
        this.setStatus('Congratulations! You won. Ending Game.');
        this.endGame();
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedAttackCoin = await this.AttackCoin.deployed();
      const playerAttack = await deployedAttackCoin.balanceOf.call(this.model.opponent);
      this.enemy.attack = playerAttack.toString(10);
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedDefenseCoin = await this.DefenseCoin.deployed();
      const playerDefense = await deployedDefenseCoin.balanceOf.call(this.model.opponent);
      this.enemy.defense = playerDefense.toString(10);
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedAccuracyCoin = await this.AccuracyCoin.deployed();
      const playerAccuracy = await deployedAccuracyCoin.balanceOf.call(this.model.opponent);
      this.enemy.accuracy = playerAccuracy.toString(10);
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
    try {
      const deployedStaminaCoin = await this.StaminaCoin.deployed();
      const playerStamina = await deployedStaminaCoin.balanceOf.call(this.model.opponent);
      this.enemy.stamina = playerStamina.toString(10);
    } catch (e) {
      console.log(e);
      this.setStatus('Error; see log.');
    }
  }

  async executeAttack(attackId) {
    try {
      const deployedGameMaker = await this.GameMaker.deployed();
      const executedAttack = await deployedGameMaker.executeAttack.call(attackId, this.matchId, this.model.account,
        this.model.opponent);
      await deployedGameMaker.executeAttack.sendTransaction(attackId, this.matchId, this.model.account,
        this.model.opponent, { from: this.model.account });
    } catch (e) {
      console.log(e);
      this.setStatus('Error executing attack; see log.');
    }
  }

  async getAttacks() {
    if (!this.AttackPool) {
      this.setStatus('AttackPool is not loaded, unable to send transaction');
      return;
    }
    console.log('Searching Game');

    try {
      const deployedProfilePool = await this.ProfilePool.deployed();
      const transaction = await deployedProfilePool.getAllProfilesPart2.call();

      if (!transaction) {
        this.setStatus('Request failed!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error getting attacks; see log.');
    }
  }

  async listenAttackEvents() {
    const deployedGameMaker = await this.GameMaker.deployed();
    deployedGameMaker.attackResult((err, res) => {
      if (err) {
        console.log('EVENT ERROR: ' + err);
      } else {
        console.log('ATTACK MINED!!!');
        console.log(res);
        if (+res.args.info[res.args.info.length - 1].toString(10) === 1) {
          if (res.args.me === this.model.account.toLowerCase()) {
               this.setStatus('Attack successful!');
          } else {
            this.setStatus('Opponent successfully attacked you!');
          }
        } else {
          if (res.args.me === this.model.account.toLowerCase()) {
            this.setStatus('Your attack was cancelled!');
       } else {
         this.setStatus('Opponent attack was cancelled!');
       }
        }
        this.getStats();
      }
    });
  }

  async endGame() {
    const players = [this.model.account, this.model.opponent];
    console.log('Ending Game!');
    console.log(players);
    try {
      const deployedGameMaker = await this.GameMaker.deployed();
      // await deployedGameMaker.endGame.call(players);
      await deployedGameMaker.endGame.sendTransaction(players, { from: this.model.account });
      alert('Game Ended');
      await this.getStats();
      this.router.navigate(['/lobby']);
    } catch (e) {
      console.log(e);
      this.setStatus('Error ending game; see log.');
    }
  }

}
