import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../util/web3.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router/';
import { forEach } from '../../../node_modules/@angular/router/src/utils/collection';
import value from '*.json';
const gameMaker_artifacts = require('../../../build/contracts/GameMaker.json');
const profilePool_artifacts = require('../../../build/contracts/ProfilePool.json');
const gamePlaner_artifacts = require('../../../build/contracts/GamePlaner.json');

declare let require: any;

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})
export class LobbyComponent implements OnInit {

  accounts: string[];
  MetaCoin: any;
  GameMaker: any;
  ProfilePool: any;
  GamePlaner: any;
  waitingForGame: Boolean = false;
  selectedProfile = 0;
  selectedProfile2 = 0;
  matchInfoEvent;



  status = '';

  model = {
    amount: 5,
    receiver: '',
    balance: 0,
    account: '',
    profiles: [{ name: 'placeholder', id: 0, health: 0, attack: 0, defense: 0, accuracy: 0, stamina: 0 }],
    enemy: ''
  };

  constructor(private web3Service: Web3Service, private matSnackBar: MatSnackBar, private router: Router) {
    console.log('Constructor: ' + web3Service);
  }


  ngOnInit(): void {
    console.log('OnInit: ' + this.web3Service);
    console.log(this);
    this.web3Service.artifactsToContract(gameMaker_artifacts)
      .then((GameMakerAbstraction) => {
        this.GameMaker = GameMakerAbstraction;
        this.listenStartGameEvent();
      });
    this.web3Service.artifactsToContract(profilePool_artifacts)
      .then((ProfilePoolAbstraction) => {
        this.ProfilePool = ProfilePoolAbstraction;
        this.testProfilePool();
      });
    this.web3Service.artifactsToContract(gamePlaner_artifacts)
      .then((GamePlanerAbstraction) => {
        this.GamePlaner = GamePlanerAbstraction;
        this.listenMatchInfoEvent();
      });
    this.watchAccount();
  }

  async listenMatchInfoEvent() {
    const deployedGamePlaner = await this.GamePlaner.deployed();
    deployedGamePlaner.MatchInfo((err, res) => {
      if (err) {
        console.log('EVENT ERROR: ' + err);
      } else {
        console.log('MATCH INFO!!!');
        console.log(res);
      }
  });
}

  async listenStartGameEvent() {
    const deployedGameMaker = await this.GameMaker.deployed();
    let enemy;
    let account;
    let enemyProfile;
    let playerProfile;
    let matchId;
    deployedGameMaker.GameStarted((err, res) => {
      if (err) {
        console.log('EVENT ERROR: ' + err);
      } else {
        console.log('GAME FOUND!!!');
        console.log(res);
        matchId = res.args.matchCount.toString(10);
        res.args.players.forEach((player, idx) => {
          console.log(player);
          if (player !== this.model.account.toLowerCase()) {
            console.log('enemy detected');
            enemy = player;
            enemyProfile = res.args.profiles[idx].toString(10);
          } else if (player === this.model.account.toLowerCase()) {
            console.log('player detected');
            account = player;
            playerProfile = res.args.profiles[idx].toString(10);
          }
        });
        if (enemy && account) {
        this.waitingForGame = false;
        this.router.navigate(['/game'], {
          queryParams: {
            'enemy': enemy,
            'matchId': matchId,
            'account': account,
            'enemyProfile': enemyProfile,
            'playerProfile': playerProfile
          }
        });
      }
      }
    });
  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }

  async getProfileInfo(identifier) {
    const deployedProfilePool = await this.ProfilePool.deployed();
    const profile = await deployedProfilePool.getInfo.call(identifier);
    this.model.profiles[identifier] = {
      id: identifier,
      name: profile[1].toString(10),
      health: profile[2].toString(10),
      attack: profile[3].toString(10),
      defense: profile[4].toString(10),
      accuracy: profile[5].toString(10),
      stamina: profile[6].toString(10),
    };
    console.log(this.model.profiles);
    this.selectedProfile = 0;
  }

  async testProfilePool() {
    try {
      const deployedProfilePool = await this.ProfilePool.deployed();
      console.log(deployedProfilePool);
      const transaction = await deployedProfilePool.getIdentifiers.call();
      if (!transaction) {
        this.setStatus('Request failed!');
      } else {
        console.log(transaction);
        transaction.forEach((identifier) => {
          this.getProfileInfo(identifier.toString(10));
        });
        this.selectedProfile = 0;
        this.selectedProfile2 = 0;
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error finding game; see log.');
    }
  }

  watchAccount() {
    this.web3Service.accountsObservable.subscribe((accounts) => {
      console.log(accounts);
      this.accounts = accounts;
      this.model.account = accounts[0];
      // this.model.player2 = accounts[1];
    });
  }

  async checkForMatch() {
    if (this.waitingForGame) {
    // CALL CONTRACT
    try {
      const deployedGamePlaner = await this.GamePlaner.deployed();
      console.log(deployedGamePlaner);
      const requestMatch = await deployedGamePlaner.updateTimestamp.call(this.model.account, this.selectedProfile);
      await deployedGamePlaner.updateTimestamp.sendTransaction(this.model.account, this.selectedProfile,
        { from: this.model.account });
      setTimeout(() => {
        this.checkForMatch();
      }, 15000);
    } catch (e) {
      console.log(e);
      this.setStatus('Error updating matchmaker; see log.');
    }
  }
  }


  async searchGame() {
    if (!this.GamePlaner) {
      this.setStatus('GamePlaner is not loaded, unable to send transaction');
      return;
    }
    console.log('Searching Game');

    try {
      const deployedGamePlaner = await this.GamePlaner.deployed();
      console.log(deployedGamePlaner);
      const findMatch = await deployedGamePlaner.matchMe.call(this.model.account, this.selectedProfile);
      await deployedGamePlaner.matchMe.sendTransaction(this.model.account, this.selectedProfile,
        { from: this.model.account });
        console.log('Waiting!');
        this.setStatus('Looking for a new game. Please Wait!');
        this.waitingForGame = true;
        setTimeout(() => {
          this.checkForMatch();
        }, 15000);
    } catch (e) {
      console.log(e);
      this.setStatus('Error finding game; see log.');
    }
  }

  async startTestGame() {
    if (!this.GameMaker) {
      this.setStatus('GameMaker is not loaded, unable to send transaction');
      return;
    }
    console.log('Searching Game');

    try {
      const deployedGameMaker = await this.GameMaker.deployed();
      console.log(deployedGameMaker);
      const players = [this.model.account, this.model.enemy];
      console.log(players);
      const startGame = await deployedGameMaker.startGame.call(players, [this.selectedProfile, this.selectedProfile2]);
      deployedGameMaker.startGame.sendTransaction(players, [this.selectedProfile,
         this.selectedProfile2], { from: this.model.account });
    } catch (e) {
      console.log(e);
      this.setStatus('Error finding game; see log.');
    }
  }
}
