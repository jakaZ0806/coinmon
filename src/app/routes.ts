import { Routes, RouterModule } from '@angular/router/';

import { LobbyComponent } from './lobby/lobby.component';
import { GameComponent } from './game/game.component';



export const routes: Routes = [
  {path: '', redirectTo: '/lobby', pathMatch: 'full'},
  {path: 'game', component: GameComponent},
  {path: 'lobby', component: LobbyComponent}
];

export const routing = RouterModule.forRoot(routes);
